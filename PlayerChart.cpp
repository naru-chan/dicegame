#include "PlayerChart.h"


PlayerChart::PlayerChart()
{
	ones = StaticValues::NULL_INT;
	twos = StaticValues::NULL_INT;
	threes = StaticValues::NULL_INT;
	fours = StaticValues::NULL_INT;
	fives = StaticValues::NULL_INT;
	sixes = StaticValues::NULL_INT;

	upperBonus = 0;
	upperTotal = 0;

	threeOfAKind = StaticValues::NULL_INT;
	fourOfAKind = StaticValues::NULL_INT;
	fullHouse = StaticValues::NULL_INT;
	smallStraight = StaticValues::NULL_INT;
	largeStraight = StaticValues::NULL_INT;
	yahtzee = StaticValues::NULL_INT;
	chance = StaticValues::NULL_INT;

	lowerTotal = 0;
	grandTotal = 0;

	lowerFilled = false;
	upperFilled = false;
	complete = false;
};


PlayerChart::~PlayerChart()
{
}


void PlayerChart::checkUpper() {
	bool upperFilled = true;
	for (int i = StaticValues::UPPER_FIELD_MIN; i <= StaticValues::UPPER_FIELD_MAX; i++) {
		if (!fieldFlags.test(i)) {
			upperFilled = false;
			break;
		}
	}

	if (upperFilled) {
		this->upperFilled = upperFilled;
		if (upperBonus == StaticValues::NULL_INT) {
			checkUpperBonus();
		}
		checkTotal();
	};
};

void PlayerChart::checkLower() {
	bool lowerFilled = true;
	for (int i = StaticValues::LOWER_FIELD_MIN; i <= StaticValues::LOWER_FIELD_MAX; i++) {
		if (!fieldFlags.test(i)) {
			lowerFilled = false;
			break;
		}
	}

	if (lowerFilled) {
		this->lowerFilled = lowerFilled;
		checkTotal();
	}
}

void PlayerChart::checkUpperBonus() {
	if (upperTotal == StaticValues::UPPER_BONUS_CONDITION) {
		upperBonus = StaticValues::UPPER_BONUS;
		upperTotal += upperBonus;
	}
}

void PlayerChart::checkTotal() {
	complete = upperFilled && lowerFilled;
}

bool PlayerChart::setField(StaticValues::Field field, int value) {
	switch (field) {
	case StaticValues::Field::ONES:
		if (ones == StaticValues::NULL_INT) {
			ones = value;
			fieldFlags.set(StaticValues::ONES_INDEX);
			upperTotal += value;
			grandTotal += value;
			checkUpper();
			return true;
		}
		return false;
	case StaticValues::Field::TWOS:
		if (twos == StaticValues::NULL_INT) {
			twos = value;
			fieldFlags.set(StaticValues::TWOS_INDEX);
			upperTotal += value;
			grandTotal += value;
			checkUpper();
			return true;
		}
		return false;
	case StaticValues::Field::THREES:
		if (threes == StaticValues::NULL_INT) {
			threes = value;
			fieldFlags.set(StaticValues::THREES_INDEX);
			upperTotal += value;
			grandTotal += value;
			checkUpper();
			return true;
		}
		return false;
	case StaticValues::Field::FOURS:
		if (fours == StaticValues::NULL_INT) {
			fours = value;
			fieldFlags.set(StaticValues::FOURS_INDEX);
			upperTotal += value;
			grandTotal += value;
			checkUpper();
			return true;
		}
		return false;
	case StaticValues::Field::FIVES:
		if (fives == StaticValues::NULL_INT) {
			fives = value;
			fieldFlags.set(StaticValues::FIVES_INDEX);
			upperTotal += value;
			grandTotal += value;
			checkUpper();
			return true;
		}
		return false;
	case StaticValues::Field::SIXES:
		if (sixes == StaticValues::NULL_INT) {
			sixes = value;
			fieldFlags.set(StaticValues::SIXES_INDEX);
			upperTotal += value;
			grandTotal += value;
			checkUpper();
			return true;
		}
		return false;
	case StaticValues::Field::THREE_OF_A_KIND:
		if (threeOfAKind == StaticValues::NULL_INT) {
			threeOfAKind = value;
			fieldFlags.set(StaticValues::THREE_OF_A_KIND_INDEX);
			lowerTotal += value;
			grandTotal += value;
			checkLower();
			return true;
		}
		return false;
	case StaticValues::Field::FOUR_OF_A_KIND:
		if (fourOfAKind == StaticValues::NULL_INT) {
			fourOfAKind = value;
			fieldFlags.set(StaticValues::FOUR_OF_A_KIND_INDEX);
			lowerTotal += value;
			grandTotal += value;
			checkLower();
			return true;
		}
		return false;
	case StaticValues::Field::FULL_HOUSE:
		if (fullHouse == StaticValues::NULL_INT) {
			fullHouse = value;
			fieldFlags.set(StaticValues::FULL_HOUSE_INDEX);
			lowerTotal += value;
			grandTotal += value;
			checkLower();
			return true;
		}
		return false;
	case StaticValues::Field::SMALL_STRAIGHT:
		if (smallStraight == StaticValues::NULL_INT) {
			smallStraight = value;
			fieldFlags.set(StaticValues::SMALL_STRAIGHT_INDEX);
			lowerTotal += value;
			grandTotal += value;
			checkLower();
			return true;
		}
		return false;
	case StaticValues::Field::LARGE_STRAIGHT:
		if (largeStraight == StaticValues::NULL_INT) {
			largeStraight = value;
			fieldFlags.set(StaticValues::LARGE_STRAIGHT_INDEX);
			lowerTotal += value;
			grandTotal += value;
			checkLower();
			return true;
		}
		return false;
	case StaticValues::Field::YAHTZEE:
		if (yahtzee == StaticValues::NULL_INT) {
			yahtzee = value;
			fieldFlags.set(StaticValues::YAHTZEE_INDEX);
			lowerTotal += value;
			grandTotal += value;
			checkLower();
			return true;
		}
		return false;
	case StaticValues::Field::CHANCE:
		if (chance == StaticValues::NULL_INT) {
			chance = value;
			fieldFlags.set(StaticValues::CHANCE_INDEX);
			lowerTotal += value;
			grandTotal += value;
			checkLower();
			return true;
		}
		return false;
	}
}

void PlayerChart::print() {
	std::cout << "Ones: " << ones << std::endl;
	std::cout << "Twos: " << twos << std::endl;
	std::cout << "Threes: " << threes << std::endl;
	std::cout << "Fours: " << fours << std::endl;
	std::cout << "Fives: " << fives << std::endl;
	std::cout << "Sixes: " << sixes << std::endl;
	std::cout << "Bonus: " << upperBonus << std::endl;
	std::cout << "Upper Total: " << upperTotal << std::endl << std::endl;

	std::cout << "Three Of A Kind: " << threeOfAKind << std::endl;
	std::cout << "Four Of A Kind: " << fourOfAKind << std::endl;
	std::cout << "Full House: " << fullHouse << std::endl;
	std::cout << "Small Straight: " << smallStraight << std::endl;
	std::cout << "Large Straight: " << largeStraight << std::endl;
	std::cout << "Yahtzee: " << yahtzee << std::endl;
	std::cout << "Chance: " << chance << std::endl;
	std::cout << "Lower Total: " << lowerTotal << std::endl << std::endl;

	std::cout << "Grand Total: " << grandTotal << std::endl << std::endl;
}
