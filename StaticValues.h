#pragma once
class StaticValues
{

public:

	enum Field {
		ONES = 1, TWOS, THREES, FOURS, FIVES, SIXES,
		THREE_OF_A_KIND, FOUR_OF_A_KIND, FULL_HOUSE, 
		SMALL_STRAIGHT, LARGE_STRAIGHT, YAHTZEE, CHANCE
	};

	static const int NULL_INT = -1;
	static const int UPPER_BONUS_CONDITION = 63;
	static const int UPPER_BONUS = 35;
	static const int SMALL_STRAIGHT_VALUE = 30;
	static const int LARGE_STRAIGHT_VALUE = 40;
	static const int FULL_HOUSE_VALUE = 25;
	static const int YAHTZEE_VALUE = 50;

	static const int ONES_INDEX = 0;
	static const int TWOS_INDEX = 1;
	static const int THREES_INDEX = 2;
	static const int FOURS_INDEX = 3;
	static const int FIVES_INDEX = 4;
	static const int SIXES_INDEX = 5;

	static const int UPPER_FIELD_MIN = 0;
	static const int UPPER_FIELD_MAX = 5;

	static const int THREE_OF_A_KIND_INDEX = 6;
	static const int FOUR_OF_A_KIND_INDEX = 7;
	static const int FULL_HOUSE_INDEX = 8;
	static const int SMALL_STRAIGHT_INDEX = 9;
	static const int LARGE_STRAIGHT_INDEX = 10;
	static const int YAHTZEE_INDEX = 11;
	static const int CHANCE_INDEX = 12;

	static const int LOWER_FIELD_MIN = 6;
	static const int LOWER_FIELD_MAX = 12;
	static const int FIELD_COUNT = 13;

	static const int UPPER = 0;
	static const int LOWER = 1;
	static const int PART_COUNT = 2;

	static const int PIP_ONE = 1;
	static const int PIP_TWO = 2;
	static const int PIP_THREE = 3;
	static const int PIP_FOUR = 4;
	static const int PIP_FIVE = 5;
	static const int PIP_SIX = 6;

	static const int DICE_COUNT = 5;
	static const int MAX_DICE_VALUE = 6;

	StaticValues();

	~StaticValues();
};

