#pragma once

#include <iostream>
#include <time.h>
#include <algorithm>
#include <vector>
#include "StaticValues.h"

using std::vector;

class Throw
{

private:
	int dice1;
	int dice2;
	int dice3;
	int dice4;
	int dice5;

public:

	Throw();

	~Throw();

	vector<int> getPossibleFields();

	void print();

private:

	int getFieldValue(StaticValues::Field field);

	int getUpperFieldValue(StaticValues::Field field);

	int getKindValue(StaticValues::Field field);

	int getStraightValue(StaticValues::Field field);

	int getFullHouseValue();

	int getYahtzeeValue();

	int getTotalValue();
};

