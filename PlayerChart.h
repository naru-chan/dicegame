#pragma once

#include <iostream>
#include <bitset>
#include "StaticValues.h"

using std::bitset;

class PlayerChart
{

private:
	int ones;
	int twos;
	int threes;
	int fours;
	int fives;
	int sixes;

	int upperBonus;
	int upperTotal;
	
	int fourOfAKind;
	int threeOfAKind;
	int fullHouse;
	int smallStraight;
	int largeStraight;
	int yahtzee;
	int chance;

	int yahtzeeBonus;
	int lowerTotal;

	int grandTotal;

	std::bitset<StaticValues::FIELD_COUNT> fieldFlags;

	bool complete;
	bool upperFilled;
	bool lowerFilled;

public:
	PlayerChart(); 
	~PlayerChart();

	bool isComplete() { return complete; }

	bool setField(StaticValues::Field field, int value);

	void print();

private:

	void checkUpper();

	void checkLower();

	void checkUpperBonus();

	void checkTotal();
};

