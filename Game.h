#pragma once

#include <iostream>
#include <conio.h>

#include "PlayerChart.h"
#include "Throw.h"

class Game
{
public:
	Game();

	void start();
	~Game();

private:
	void findMax(vector<int> possibleFields, int *maxValue, int *maxIndex);
};

