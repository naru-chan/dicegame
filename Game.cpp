#include "Game.h"

Game::Game()
{
}


Game::~Game()
{
}

void Game::start() {
	PlayerChart playerChart;

	while (!playerChart.isComplete()) {
		Throw singleThrow;
		singleThrow.print();

 		vector<int> possibleFields = singleThrow.getPossibleFields();

		int maxValue = StaticValues::NULL_INT;
		int maxIndex = StaticValues::NULL_INT;
		findMax(possibleFields, &maxValue, &maxIndex);

		while (!playerChart.setField(static_cast<StaticValues::Field>(maxIndex + 1), maxValue)
				&& !playerChart.isComplete()) {
			possibleFields[maxIndex] = StaticValues::NULL_INT;
			findMax(possibleFields, &maxValue, &maxIndex);
		}

		playerChart.print();

		while (_kbhit() == 0);
		_getch();
	}
}

void Game::findMax(vector<int> possibleFields, int *maxValue, int *maxIndex) {
	int max = StaticValues::NULL_INT;
	int maxIx = StaticValues::NULL_INT;
	for (int field = 0; field < StaticValues::FIELD_COUNT; field++) {
		int currentValue = possibleFields.at(field);
		if (currentValue > max) {
			max = currentValue;
			maxIx = field;
		}
	}
	*maxValue = max;
	*maxIndex = maxIx;
	
}
