#include "Throw.h"

Throw::Throw() {
	srand(time(NULL));
	dice1 = rand() % StaticValues::DICE_COUNT + 1;
	dice2 = rand() % StaticValues::DICE_COUNT + 1;
	dice3 = rand() % StaticValues::DICE_COUNT + 1;
	dice4 = rand() % StaticValues::DICE_COUNT + 1;
	dice5 = rand() % StaticValues::DICE_COUNT + 1;

	//dice1 = 2;
	//dice2 = 5;
	//dice3 = 4;
	//dice4 = 4;
	//dice5 = 5;
}

Throw::~Throw()
{
}

int Throw::getFieldValue(StaticValues::Field field) {
	switch (field) {
	case StaticValues::Field::ONES:
	case StaticValues::Field::TWOS:
	case StaticValues::Field::THREES:
	case StaticValues::Field::FOURS:
	case StaticValues::Field::FIVES:
	case StaticValues::Field::SIXES: return getUpperFieldValue(field);
	case StaticValues::Field::THREE_OF_A_KIND:
	case StaticValues::Field::FOUR_OF_A_KIND: return getKindValue(field);
	case StaticValues::Field::FULL_HOUSE: return getFullHouseValue();
	case StaticValues::Field::SMALL_STRAIGHT:
	case StaticValues::Field::LARGE_STRAIGHT: return getStraightValue(field);
	case StaticValues::Field::YAHTZEE: return getYahtzeeValue();
	case StaticValues::Field::CHANCE: return getTotalValue();
	}
}

int Throw::getUpperFieldValue(StaticValues::Field field) {
	int total = 0;
	bool valid = false;

	if (dice1 == field) {
		total += field;
		valid = true;
	}
	if (dice2 == field) {
		total += field;
		valid = true;
	}
	if (dice3 == field) {
		total += field;
		valid = true;
	}
	if (dice4 == field) {
		total += field;
		valid = true;
	}
	if (dice5 == field) {
		total += field;
		valid = true;
	}

	if (valid) {
		return total;
	}
	return 0;
}

int Throw::getKindValue(StaticValues::Field field) {
	bool valid = false;

	int expectedKinds = field == StaticValues::Field::THREE_OF_A_KIND ? 3 : 4;

	int totalScores[StaticValues::MAX_DICE_VALUE + 1];
	for (int i = 1; i <= StaticValues::MAX_DICE_VALUE; i++) {
		totalScores[i] = 0;
	}

	totalScores[dice1] += dice1;
	totalScores[dice2] += dice2;
	totalScores[dice3] += dice3;
	totalScores[dice4] += dice4;
	totalScores[dice5] += dice5;

	int kindFound = StaticValues::NULL_INT;
	for (int i = 1; i <= StaticValues::MAX_DICE_VALUE; i++) {
		if (totalScores[i] / expectedKinds == i) {
			kindFound = i;
		}
	}

	if (kindFound != StaticValues::NULL_INT) {
		return getTotalValue();
	}
	return 0;
}

int Throw::getStraightValue(StaticValues::Field field) {
	int expectedSequenceCount = field == StaticValues::Field::SMALL_STRAIGHT ? 4 : 5;

	int dices[StaticValues::DICE_COUNT];
	dices[0] = dice1;
	dices[1] = dice2;
	dices[2] = dice3;
	dices[3] = dice4;
	dices[4] = dice5;

	std::sort(dices, dices + StaticValues::DICE_COUNT);

	int prevValue = StaticValues::NULL_INT;
 	int actualSequenceCount = 1;
	for (int i = 0; i < StaticValues::DICE_COUNT; i++) {
		if (prevValue != StaticValues::NULL_INT) {
			if (dices[i] == prevValue + 1) {
				actualSequenceCount++;
			} else {
				if (actualSequenceCount != 4) {
					actualSequenceCount = 1;
				}
			}
		}
		prevValue = dices[i];
	}

	if (actualSequenceCount == expectedSequenceCount) {
		if (field == StaticValues::Field::SMALL_STRAIGHT) {
			return StaticValues::SMALL_STRAIGHT_VALUE;;
		}
		return StaticValues::LARGE_STRAIGHT_VALUE;
	}
	return 0;
}

int Throw::getFullHouseValue() {

	int totalScores[StaticValues::MAX_DICE_VALUE + 1];
	for (int i = 1; i <= StaticValues::MAX_DICE_VALUE; i++) {
		totalScores[i] = 0;
	}

	totalScores[dice1] += dice1;
	totalScores[dice2] += dice2;
	totalScores[dice3] += dice3;
	totalScores[dice4] += dice4;
	totalScores[dice5] += dice5;

	bool twoFound = false;
	bool threeFound = false;

	for (int i = 1; i <= StaticValues::MAX_DICE_VALUE; i++) {
		if (!threeFound && totalScores[i] / i == 3) {
			threeFound = true;
			continue;
		}
		if (!twoFound && totalScores[i] / i == 2) {
			twoFound = true;
			continue;
		}
	}
	if (twoFound && threeFound) {
		return StaticValues::FULL_HOUSE_VALUE;
	}
	return 0;
}

int Throw::getYahtzeeValue() {
	if (dice1 == dice2 == dice3 == dice4 == dice5) {
		return StaticValues::YAHTZEE_VALUE;
	}
	return 0;
}


int Throw::getTotalValue() {
	return dice1 + dice2 + dice3 + dice4 + dice5;
}

vector<int> Throw::getPossibleFields() {
	vector<int> possibleFields;

	possibleFields.push_back(this->getFieldValue(StaticValues::Field::ONES));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::TWOS));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::THREES));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::FOURS));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::FIVES));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::SIXES));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::THREE_OF_A_KIND));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::FOUR_OF_A_KIND));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::FULL_HOUSE));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::SMALL_STRAIGHT));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::LARGE_STRAIGHT));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::YAHTZEE));
	possibleFields.push_back(this->getFieldValue(StaticValues::Field::CHANCE));

	return possibleFields;
}

void Throw::print() {
	std::cout << "Dice: " << dice1 << ", " << dice2 << ", " << dice3 << ", "
		<< dice4 << ", " << dice5 << std::endl;
}
